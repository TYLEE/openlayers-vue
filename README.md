# zhanbb

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

//eslint-disable-next-line

# 文件说明：
## 1， arbitrarygraphDraw
### 绘制图形  Draw
## 2， colony
### 集群 绘制图形  Cluster
## 3， drawShapes
### 绘制图形  Draw
## 4， modifyAndDraw
### 修改图形  Draw ,Modify， select ,snap
## 5， motion
### 标记动画  animating
## 6， openTrajectory
### openlayer与echart结合，实现迁徙图。飞机轨迹  EChartsLayer
## 7， dragAndDrop
### 拖动加载  Drag-and-Drop  addfeatures
## 8， popup
### 地图弹框  Overlay
## 9， animatedGIF
### 地图弹框  Animated GIF
## 10， filteringWebGL
### webGL的过滤  WebGLPointsLayer
## 11， exportPDF
### 地图pdf导出  jsPDF
## 12， flightAnimation
### 航线动画  LineString arc
## 13， freehandDrawing
### 手绘  drawing
## 14， geoJson
### 地图添加geojson数据  new GeoJSON()
## 15， geojsonIntegration
### 地图添加geojson集成数据  new GeoJSON()
## 16， gpxData
### GPX数据展示  new GPX()
## 17， hitTolerancePriority
### 命中容限优先级  hitTolerance
## 18， iconColors
### 图标颜色  setStyle()
## 19， iconModification
### 图标修改  setStyle()  modify()
## 20， immediateRendering
### 即时渲染  drawGeometry upAndDown()
## 21， kml
### kml数据格式的地图的渲染 new KML()
## 22， layeClipping
### 图层裁剪 canvas()
## 23， layerSpy
### 裁剪,穿透 canvas.clip()
## 24， layerSwipe
### 图层的卷帘 canvas.clip()
## 25， magnify
### 放大地图局部 canvas
## 26， mapExportImg
### 导出地图 img map.renderSync()  Canvas.toDataURL()
## 27， measure
### 测量面积和长度  unByKey(listener)  getArea, getLength
## 28， overviewMapControl
### 总览图控件  new OverviewMap()
## 29， sharedViews
### 共享视图  // 共同的view对象才能联动视图 new view()
## 30， styleRrenderer
### 样式渲染  renderer
## 31， stylingCanvas
### 样式渲染  带有画布渐变或画布图案的造型特征
## 32， tiileLoadEvents
### 平铺加载事件  加载地图进度条
## 33， tiledArcGISMapServer
### 加载arcgis服务地址  new TileArcGISRest
## 34， tiledWMS
### 加载WMS服务地址  new TileWMS
## 35， translateFeatures
### 转换特征  new TileWMS
## 36， turf
### turf工具的使用  turf
## 36， vectorClippingLayer
### 矢量裁剪层  base.setExtent(clipLayer.getSource().getExtent())
## 36， vectorImage
### 矢量图像层  vector() addFeature



