import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/openTrajectory',
      name: 'openTrajectory',
      component: () => import('@/views/openTrajectory/index')
    },
    {
      path: '/drawShapes',
      name: 'drawShapes',
      component: () => import('@/views/drawShapes/index')
    },
    {
      path: '/arbitrarygraphDraw',
      name: 'arbitrarygraphDraw',
      component: () => import('@/views/arbitrarygraphDraw/index')
    },
    {
      path: '/colony',
      name: 'colony',
      component: () => import('@/views/colony/index')
    },
    {
      path: '/modifyAndDraw',
      name: 'modifyAndDraw',
      component: () => import('@/views/modifyAndDraw/index')
    },
    {
      path: '/motion',
      name: 'motion',
      component: () => import('@/views/motion/index')
    },
    {
      path: '/dragAndDrop',
      name: 'dragAndDrop',
      component: () => import('@/views/dragAndDrop/index')
    },
    {
      path: '/popup',
      name: 'popup',
      component: () => import('@/views/popup/index')
    },
    {
      path: '/animatedGIF',
      name: 'animatedGIF',
      component: () => import('@/views/animatedGIF/index')
    },
    {
      path: '/customAnimation',
      name: 'customAnimation',
      component: () => import('@/views/customAnimation/index')
    },
    {
      path: '/exportPDF',
      name: 'exportPDF',
      component: () => import('@/views/exportPDF/index')
    },
    {
      path: '/filteringWebGL',
      name: 'filteringWebGL',
      component: () => import('@/views/filteringWebGL/index')
    },
    {
      path: '/flightAnimation',
      name: 'flightAnimation',
      component: () => import('@/views/flightAnimation/index')
    },
    {
      path: '/freehandDrawing',
      name: 'freehandDrawing',
      component: () => import('@/views/freehandDrawing/index')
    },
    {
      path: '/geoJson',
      name: 'geoJson',
      component: () => import('@/views/geoJson/index')
    },
    {
      path: '/geojsonIntegration',
      name: 'geojsonIntegration',
      component: () => import('@/views/geojsonIntegration/index')
    },
    {
      path: '/gpxData',
      name: 'gpxData',
      component: () => import('@/views/gpxData/index')
    },
    {
      path: '/hitTolerancePriority',
      name: 'hitTolerancePriority',
      component: () => import('@/views/hitTolerancePriority/index')
    },
    {
      path: '/iconColors',
      name: 'iconColors',
      component: () => import('@/views/iconColors/index')
    },
    {
      path: '/iconModification',
      name: 'iconModification',
      component: () => import('@/views/iconModification/index')
    },
    {
      path: '/immediateRendering',
      name: 'immediateRendering',
      component: () => import('@/views/immediateRendering/index')
    },
    {
      path: '/kml',
      name: 'kml',
      component: () => import('@/views/kml/index')
    },
    {
      path: '/layeClipping',
      name: 'layeClipping',
      component: () => import('@/views/layeClipping/index')
    },
    {
      path: '/layerSpy',
      name: 'layerSpy',
      component: () => import('@/views/layerSpy/index')
    },
    {
      path: '/layerSwipe',
      name: 'layerSwipe',
      component: () => import('@/views/layerSwipe/index')
    },
    {
      path: '/magnify',
      name: 'magnify',
      component: () => import('@/views/magnify/index')
    },
    {
      path: '/mapExportImg',
      name: 'mapExportImg',
      component: () => import('@/views/mapExportImg/index')
    },
    {
      path: '/mapbox',
      name: 'mapbox',
      component: () => import('@/views/mapbox/index')
    },
    {
      path: '/measure',
      name: 'measure',
      component: () => import('@/views/measure/index')
    },
    {
      path: '/overviewMapControl',
      name: 'overviewMapControl',
      component: () => import('@/views/overviewMapControl/index')
    },
    {
      path: '/sharedViews',
      name: 'sharedViews',
      component: () => import('@/views/sharedViews/index')
    },
    {
      path: '/styleRrenderer',
      name: 'styleRrenderer',
      component: () => import('@/views/styleRrenderer/index')
    },
    {
      path: '/stylingCanvas',
      name: 'stylingCanvas',
      component: () => import('@/views/stylingCanvas/index')
    },
    {
      path: '/tiileLoadEvents',
      name: 'tiileLoadEvents',
      component: () => import('@/views/tiileLoadEvents/index')
    },
    {
      path: '/tiledArcGISMapServer',
      name: 'tiledArcGISMapServer',
      component: () => import('@/views/tiledArcGISMapServer/index')
    },
    {
      path: '/tiledWMS',
      name: 'tiledWMS',
      component: () => import('@/views/tiledWMS/index')
    },
    {
      path: '/translateFeatures',
      name: 'translateFeatures',
      component: () => import('@/views/translateFeatures/index')
    },
    {
      path: '/turf',
      name: 'turf',
      component: () => import('@/views/turf/index')
    },
    {
      path: '/vectorClippingLayer',
      name: 'vectorClippingLayer',
      component: () => import('@/views/vectorClippingLayer/index')
    },
    {
      path: '/',
      name: 'vectorImage',
      component: () => import('@/views/vectorImage/index')
    }
  ]
})
